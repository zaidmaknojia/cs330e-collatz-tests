#!/usr/bin/env python3

# -------------------------------
# projects/collatz/TestCollatz.py
# Copyright (C) 
# Glenn P. Downing
# -------------------------------

# https://docs.python.org/3/reference/simple_stmts.html#grammar-token-assert-stmt

# -------
# imports
# -------

from io import StringIO
from unittest import main, TestCase

from Collatz import collatz_read, collatz_eval, collatz_print, collatz_solve

# -----------
# TestCollatz
# -----------


class TestCollatz (TestCase):
    # ----
    # read
    # ----

    def test_read_1(self):
        s = "1 10\n"
        i, j = collatz_read(s)
        self.assertEqual(i,  1)
        self.assertEqual(j, 10)

    def test_read_2(self):
        s = "99 100\n"
        i, j = collatz_read(s)
        self.assertEqual(i,  99)
        self.assertEqual(j, 100)

    def test_read_3(self):
        s = "5 2\n"
        i, j = collatz_read(s)
        self.assertEqual(i,  5)
        self.assertEqual(j, 2)
    
    def test_read_4(self):
        s = "15 200\n"
        i, j = collatz_read(s)
        self.assertEqual(i,  15)
        self.assertEqual(j, 200)

    

    # ----
    # eval
    # ----

    def test_eval_1(self):
        v = collatz_eval(1, 10)
        self.assertEqual(v, 20)

    def test_eval_2(self):
        v = collatz_eval(100, 200)
        self.assertEqual(v, 125)

    def test_eval_3(self):
        v = collatz_eval(201, 210)
        self.assertEqual(v, 89)

    def test_eval_4(self):
        v = collatz_eval(900, 1000)
        self.assertEqual(v, 174)

    def test_eval_5(self):
        v = collatz_eval(9998, 9999)
        self.assertEqual(v, 92)
    
    def test_eval_6(self):
        v = collatz_eval(55, 33)
        self.assertEqual(v, 113)

    def test_eval_7(self):
        v = collatz_eval(22, 22)
        self.assertEqual(v, 16)
    
    def test_eval_8(self):
        v = collatz_eval(220, 1220)
        self.assertEqual(v, 182)

    # -----
    # print
    # -----

    def test_print_1(self):
        w = StringIO()
        collatz_print(w, 1, 10, 20)
        self.assertEqual(w.getvalue(), "1 10 20\n")

    def test_print_2(self):
        w = StringIO()
        collatz_print(w, 5, 7, 8)
        self.assertEqual(w.getvalue(), "5 7 8\n")

    def test_print_3(self):
        w = StringIO()
        collatz_print(w, 20, 29, 40)
        self.assertEqual(w.getvalue(), "20 29 40\n")
    
    def test_print_4(self):
        w = StringIO()
        collatz_print(w, 90, 99, 400)
        self.assertEqual(w.getvalue(), "90 99 400\n")
    


    # -----
    # solve
    # -----

    def test_solve_1(self):
        r = StringIO("1 10\n100 200\n201 210\n900 1000\n")
        w = StringIO()
        collatz_solve(r, w)
        self.assertEqual(
            w.getvalue(), "1 10 20\n100 200 125\n201 210 89\n900 1000 174\n")

    def test_solve_2(self):
        r = StringIO("2 40\n200 700\n1000 1001\n7 90\n")
        w = StringIO()
        collatz_solve(r, w)
        self.assertEqual(
            w.getvalue(), "2 40 112\n200 700 145\n1000 1001 143\n7 90 116\n")

    def test_solve_3(self):
        r = StringIO("3 6\n80 90\n500 600\n900 999\n")
        w = StringIO()
        collatz_solve(r, w)
        self.assertEqual(
            w.getvalue(), "3 6 9\n80 90 111\n500 600 137\n900 999 174\n")
    
    def test_solve_4(self):
        r = StringIO("6 9\n83 95\n1500 1600\n9000 9990\n")
        w = StringIO()
        collatz_solve(r, w)
        self.assertEqual(
            w.getvalue(), "6 9 20\n83 95 111\n1500 1600 167\n9000 9990 260\n")


# ----
# main
# ----

if __name__ == "__main__":
    main()

""" #pragma: no cover
$ coverage run --branch TestCollatz.py >  TestCollatz.out 2>&1


$ cat TestCollatz.out
.......
----------------------------------------------------------------------
Ran 7 tests in 0.000s
OK


$ coverage report -m                   >> TestCollatz.out



$ cat TestCollatz.out
.......
----------------------------------------------------------------------
Ran 7 tests in 0.000s
OK
Name             Stmts   Miss Branch BrPart  Cover   Missing
------------------------------------------------------------
Collatz.py          12      0      2      0   100%
TestCollatz.py      32      0      0      0   100%
------------------------------------------------------------
TOTAL               44      0      2      0   100%
"""